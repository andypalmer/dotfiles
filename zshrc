# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

source ~/.zplug/init.zsh

zplug romkatv/powerlevel10k, as:theme, depth:1
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

zplug "AndyPalmer/zsh-preferences", from:gitlab, use:"preferences/*"
zplug "AndyPalmer/zsh-preferences", from:gitlab, as:command, use:"bin/*"

zplug "RiverGlide/zsh-goenv", from:gitlab
zplug "RiverGlide/zsh-nodenv", from:gitlab
zplug "RiverGlide/zsh-pyenv", from:gitlab
zplug "RiverGlide/zsh-rbenv",  from:gitlab
zplug "RiverGlide/zsh-rbenv-vars",  from:gitlab

zplug "chrissicool/zsh-256color"
zplug "jreese/zsh-titles"

zplug "junegunn/fzf", as:command, hook-build:"./install --bin", use:"bin/*"
zplug "junegunn/fzf", use:"shell/*.zsh"

zplug "wookayin/fzf-fasd"

zplug "zdharma/fast-syntax-highlighting", defer:2

zplug "RiverGlide/zsh-iterm2", from:gitlab, defer:2
zplug "RiverGlide/zsh-iterm2", from:gitlab, as:command, use:"utilities/*"

zplug "AndyPalmer/git-hooks", from:gitlab, as:command, use:"git-hooks"
zplug "zdharma-continuum/zsh-diff-so-fancy", as:command, use:"bin/*"
zplug "arzzen/git-quick-stats", as:command, use:"git-quick-stats"
zplug "paulirish/git-open", as:plugin

zplug "laurent22/rsync-time-backup", as:command, use:"rsync_tmbackup.sh", rename-to:"rsync_tmbackup"

zplug 'zplug/zplug', hook-build:'zplug --self-manage'
zplug load
