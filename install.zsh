#!/usr/local/bin/zsh
for f in ${0:a:h}/*; do
  if [[ $f == ${0:a} ]]; then continue; fi
  ln -s $f ~/.$(basename $f);
done
